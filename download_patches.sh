#!/bin/bash
filename='current_patches.csv'
suffix='psr4-reroll'

files=`cat $filename`
for line in $files
do
  IFS=', ' read -a array <<< "$line"
  nid=${array[0]}
  filepath=${array[1]}
  curl https://drupal.org/$filepath -o "patches/$nid-$suffix.patch"
done
