# Enter your absolute paths here.
patches=/Users/jess.myrbo/git/patch_rerolls/patches
does_not_apply=/Users/jess.myrbo/git/patch_rerolls/does_not_apply
needs_reroll=/Users/jess.myrbo/git/patch_rerolls/needs_reroll
rerolled=/Users/jess.myrbo/git/patch_rerolls/rerolled
apply_ref=pre-psr4
rebase_ref=post-psr4

# First sort out which patches already didn't apply, before rebasing.
git checkout $apply_ref
for p in `ls $patches/*`
do
  o=`git apply --index $p 2>&1`
  if [ $? != 0 ]
    then
      mv $p $does_not_apply
  fi
  git reset --hard &> /dev/null
done
echo `ls -1 $does_not_apply | wc -l` patches did not apply to HEAD. They will be skipped.

# Next, try to apply the remaining patches on top of rebase ref.
git checkout $rebase_ref
for p in `ls $patches/*`
do
  o=`git apply --index $p 2>&1`
  if [ $? != 0 ]
    then
      mv $p $needs_reroll
    else
      find core/modules | grep lib$
      find code/modules | grep tests/Drupal$
      if [ $? != 0 ]
        then
          mv $p $needs_reroll
      fi
  fi
  git reset --hard &> /dev/null
done
echo `ls -1 $needs_reroll | wc -l` patches need rerolls.

# Now, for each patch that needs a reroll, try to rebase.
for p in `ls $needs_reroll/*`
do
  # First branch off 8.x to apply the patch.
  git checkout -b tmp $apply_ref &> /dev/null
  git apply --index $p &> /dev/null
  git commit -am tmp &> /dev/null

  # Now try to rebase it.
  o=`git rebase $rebase_ref`
  if [ $? != 0 ]
    then
      echo Rebasing $p failed.
      rm -rf .git/rebase-apply
    else
      git diff $rebase_ref > $rerolled/`basename "$p"`
  fi

  # Delete the branch to start over.
  git reset --hard &> /dev/null
  git checkout $apply_ref &> /dev/null
  git branch -D tmp &> /dev/null
done
echo `ls -1 $rerolled | wc -l` patches were rerolled with rebase.
git checkout 8.x &> /dev/null
git branch -D assert_t &> /dev/null
